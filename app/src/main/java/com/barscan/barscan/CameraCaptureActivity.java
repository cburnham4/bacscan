package com.barscan.barscan;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;

import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class CameraCaptureActivity extends AppCompatActivity {

    private static final String TAG = CameraCaptureActivity.class.getSimpleName();

    public static final String LICENSE_PARAM = "licenseParam";

    private CameraView cameraView;

    private DatabaseReference mDatabase;

    private ScannedLicense mockLicense = new ScannedLicense("Carl", "Burnham", 23, "04/24/1995", "male", "", "11/20/2017 08:08:08");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_capture);

        cameraView = findViewById(R.id.camera);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        setupBarcodeScanner();
    }


    private void setupBarcodeScanner() {
        FirebaseVisionBarcodeDetectorOptions options =
                new FirebaseVisionBarcodeDetectorOptions.Builder()
                        .setBarcodeFormats(
                                FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
                        .build();
    }

    private void processImage(Bitmap bitmap) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector();

        Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                        parseImageResults(barcodes);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, e.getLocalizedMessage());
                    }
                });
    }

    private void parseImageResults(List<FirebaseVisionBarcode> barcodes) {
        if(barcodes.isEmpty()) {
            Toast.makeText(this, "No barcodes detected", Toast.LENGTH_SHORT);
            returnDriverLicense(null);
            return;
        }
        for (FirebaseVisionBarcode barcode: barcodes) {
            Rect bounds = barcode.getBoundingBox();
            Point[] corners = barcode.getCornerPoints();

            String rawValue = barcode.getRawValue();

            int valueType = barcode.getValueType();
            // See API reference for complete list of supported types
            switch (valueType) {
                case FirebaseVisionBarcode.TYPE_DRIVER_LICENSE:
                    FirebaseVisionBarcode.DriverLicense license = barcode.getDriverLicense();
                    //returnDriverLicense(license);
                    Log.e(TAG, license.getFirstName());
                default:
                    returnDriverLicense(null);

            }
        }
    }


    public void scanBarcodeClicked(View view) {
        cameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
            @Override
            public void callback(CameraKitImage cameraKitImage) {
                processImage(cameraKitImage.getBitmap());
            }
        });
    }

    private void showDialog(ScannedLicense scannedLicense) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        String userInfo = "";
        if(scannedLicense.getAge() < 21) {
            userInfo = "Person Is not 21 \n";
        }
        userInfo += scannedLicense.getUserInfo();
        alertDialogBuilder.setTitle("User Information").setMessage(userInfo);
                alertDialogBuilder.setPositiveButton("Okay",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                finish();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void pushMockData() {
        for (int i = 0; i < 200; i++) {
            ScannedLicense scannedId = getMockLicense();
            pushData(scannedId);
        }
    }

    private void pushData(ScannedLicense scannedId) {
        mDatabase.child("customers").child(scannedId.getId()).setValue(scannedId).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.e(TAG, task.toString());
            }
        });
    }

    private void returnDriverLicense(FirebaseVisionBarcode.DriverLicense driverLicense) {
        Intent data = new Intent();

        ScannedLicense scannedId = mockLicense;

        data.putExtra(LICENSE_PARAM, scannedId);
        mDatabase.child("customers").child(scannedId.getId()).setValue(scannedId);

        showDialog(mockLicense);
//        setResult(RESULT_OK, data);
//        finish();
    }

    private ScannedLicense getMockLicense() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return new ScannedLicense(RandomStringUtils.randomAlphanumeric(5), RandomStringUtils.randomAlphanumeric(8), 23, "04/24/1995",
                "male", "", DateHelper.getRandomTime());
    }


    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

}
